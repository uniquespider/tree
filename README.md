# tree插件说明

### 简单上手

```aidl
<!-- 首先需要引入css样式与js文件（路径可能有所不同） -->
<link href="css/tree.css" rel="stylesheet">
<script src="js/tree.js" type="text/javascript"></script>
<!-- 创建一个div，设置它的宽度、边框 -->
<div id="main" style="
      width: 300px;
      border: 1px solid black;
      margin: 20px 30px;
      ">
树状菜单
</div>
<script>
//创建一个数据源json数组
var data = [{
    name: "父类",
    pid:'1',
    childs: [{
      name: "子类",
      data:'你好',
      title:'child',
      childs: [{
        name: "子子类",
        unfold: true,
        data:'你好,子子类',
        childs: [{
          name: "子子子类",
          d:'你好,子子子类',
          hide:true
        }]
      }]
    }, {
      name: "子类1",
      unfold: false,
      data:'你好,子类1',
      childs: [{name: "子子类1"}]
    },{
      name:'子类2',
      type:'folder'
    }]
  }];
 //设置参数
 var option={
        data:data,
        elem:'#main'
        };
 var tree=new Tree(option);
 tree.init();
</script>
```

### 加载数据方式(loadAll)
```aidl
 var option={
      data:[{
            tree_id:0,
            tree_name:'name',
            tree_title:'';
            list:[...]
          },...],
      elem:'#main',
      //loadAll默认为true
      loadAll:false,
      //强制不生成id，若id不存在或为无效数据，null、undefined，则仍然会生成id
      compelCustom:true
      };
```
##### 若loadAll参数为true，则init时全部加载完成，若loadAll为false，则init时只加载一层。其余只会在节点展开时加载。若数据量大，则建议将该值设置为false
##### 注意：该参数不会影响checkbox选项，选中全部即选中全部，包含未加载到dom树上的节点。
###### id生成方式：理论上整个tree插件只能存在一个id，不可出现重复id，但为了更好的适应项目中，id可用户自定义，即下面会介绍到的format参数。为了保证id唯一性，将会在生成节点时，判断节点中是否已经含有该id，若id存在，则会自动生成一个唯一id。可通过compelCustom参数指定强制不生成id，但是如果节点数据内的id属性不存在，则仍会自动生成一个id给该节点。
###### 因此，为了checkbox组件获取选中节点的效率，若剩余的节点若没有加载到dom树中，id如果出现重复，不会重新生成，则获取到的选中节点的数量会改变。

### 数据源json数组参数列表

| 参数名 | 类型 | 默认值 | 示例 | 描述 |
| --- | --- | --- | --- | --- |
| name | string | 节点 | "父类" | 显示在外面的文字 |
| id | object | 自动生成，从0开始 | 1 | 标识，可通过下面的getNodeById(方法获取节点)|
|title | string | 与name属性一致|"这是描述"|鼠标悬浮在节点上的描述文字
|unfold | boolean|false|true|如果为true，则该节点下面的子节点展开，且父节点也会展开|
|data|object|无|"这是数据"|保存在节点上的数据，每次节点触发事件的回调都会将该字段的内容传入|
|type|boolean或string|false|true或folder或false|表示该节点是否为文件夹节点|
|hide|boolean|false|true|表示改节点是否隐藏，如果为true，则该节点与该节点下的所有子节点都会隐藏|
|childs|array|[]|[{name:'...',...}]|该节点下面的子节点，格式与此表数据格式一致，理论上可套用无限层，但可能造成浏览器堆栈溢出。|

如果数据源的参数名与默认的参数名不一致，则需在option内加入format参数，该参数的调用在init方法之前，可通过更改传入的format参数进行更改

若后续数据需要更改，则可在调用addNode方法之前，通过 ```tree.format.属性名```的方式设置对应参数名，也可以通过保存format对象，后续更改format对象进行修改
### 自定义data格式(format)
```aidl
//示例
var option={
    data:[{
        tree_id:0,
        tree_name:'name',
        tree_title:'';
        list:[...]
    },...],
    elem:'#main',
    format:function (format){
       //数据源代表id的参数名
      format.id='tree_id';
        //若想要将该节点所有内容全部作为data（子节点的数据除外），需要将format.data的值设置为all
      format.data='all';
      format.name='tree_name';
      format.title='tree_title';
      format.childs='list';
    }
}

```
##### 注意：若无其他需求，尽量不要在外面修改format值，以防止数据错误

### 对接接口异步获取数据(async、loadText)
```aidl
//示例
 var option = {
    async:{
      //设置url
      url:'http://10.0.42.220:8090/test/treeTest',
      method:'GET',//请求方式
      data:'需要传过去的数据'
    },
    elem: '#main',
    loadText:''//加载阶段显示的文字，可设置为图片或其他
    ...
 }
```
### 工具栏(toolbar、toolClick)
```aidl
var option = {
    data:[{
        tree_id:0,
        tree_name:'name',
        tree_title:'';
        list:[...]
    },...],
    toolbar:false,
    toolClick:function (type,node){
      //type为预设的点击按钮类型，node为该节点返回的对象
    },
    //文件夹工具栏，可定义多个，可不填写，默认三个按钮[添加、修改、删除]，文件工具栏则只有[修改、删除]
    foldTools:[{'name':'删除',"title":'删除该节点',type:'delete'}],
    //文件工具栏，可不填写，默认与文件夹工具栏一直
    tools:[{'name':'删除',"title":'删除该节点',type:'delete'}]
    ...
}
//由于iE浏览器显示的工具栏是乱码的，因此如果是ie浏览器，则关闭工具栏
if(window.ActiveXObject || "ActiveXObject" in window){
  option.toolbar=false;
}

```


### 复选框(checkbox)
```aidl
var option = {
    data:[{
        tree_id:0,
        tree_name:'name',
        tree_title:'';
        list:[...]
    },...],
    checkbox:true
    ...
}
//获取所有选中的id，如果子节点全部选中，则包括父节点的也全部选中
tree.getChecked()

```

### 模板化（templet数据前置处理）

```aidl
var option = {
    data:[{
        tree_id:0,
        tree_name:'name',
        tree_title:'';
        list:[...]
    },...],
    template:function(data){
       //在这里改变data的值，改变之后会同步更新到节点上去
    }
    ...
}

```

### 事件监听回调(fold、unfold、Event)
```aidl
var option={
    unfold:function (node,tree){
      //这里写收缩后的回调内容
    },fold:function (node,tree){
      //这里写展开后的回调内容
    },prevFold:function (node,tree){
      //这里写展开前的回调内容
    },loadCallBack:function (tree){
      //这里写异步加载完成之后的回调内容
    },node_click:function(node,tree){
       //这里写子节点点击之后的回调，不包括文件夹
    },node_dbclick:function(node,tree){
        //这里写子节点双击之后的回调，不包括文件夹
    },node_mouseleave:function(node,tree){
        //这里写子节点悬浮之后的回调，不包括文件夹
    },node_mouseenter:function(node,tree){
        //这里写子节点悬浮离开之后的回调，不包括文件夹
    },folder_click:function(node,tree){
       //这里写文件夹点击之后的回调，不包括子节点
    },folder_dbclick:function(node,tree){
        //这里写文件夹双击之后的回调，不包括子节点
    },folder_mouseleave:function(node,tree){
        //这里写文件夹悬浮之后的回调，不包括子节点
    },folder_mouseenter:function(node,tree){
        //这里写文件夹悬浮离开之后的回调，不包括子节点
    },gm_click:function(node,tree){
       //这里写点击之后的回调，包括子节点
    },gm_dbclick:function(node,tree){
        //这里写双击之后的回调，包括子节点
    },gm_mouseleave:function(node,tree){
        //这里写悬浮之后的回调，包括子节点
    },gm_mouseenter:function(node,tree){
        //这里写悬浮离开之后的回调，包括子节点
    }
    elem: '#main',
    ...
}
```
以上所有的node为触发事件的node对象，tree为树对象

### 可调用的方法
|方法名 |参数|示例 |返回内容|<div style="width:200px">示例</div>|<div style="width:200px">描述</div>|<div style="width:200px">参数详解</div>|
|---|---|---|---|---|---|---|
|init|无|init()|无|无|创建节点并初始化，执行完毕后调用加载回调，只调用一次，否则会出现重复数据，异步加载数据时无需调用|
|getNodeById|id|getNodeById(0)|Node|\<div ...\>\...</div\>(dom节点)|通过id获取节点，即使id重复，也仅返回第一个id，查找id的顺序为父节点--父节点下第一个子节点--父节点下第二个子节点...--第一个子节点下的子节点1...第二个子节点下的子节点...--
|addNodeByInterface|node,option,func|addNodeByInterface(getNodeById(0),{async},true,{回调函数})|无|无|通过调用接口创建节点|node:要追加到的节点，需要使用getNodeById()方法获取。option:与异步加载数据的async参数一致。func:加载完成之后的回调函数
|addNode|node,datas|addNode(getNodeById(0),data,true)|无|无|直接将数据加载到指定节点下|node:略，nodes:与同步加载的data参数一致
|nodeUnfold|node,call,unfold|nodeUnfold(getNodeById(0),false,true)|无|无|将指定节点闭合，执行此操作之后，此节点下所有文件夹全部闭合|node:略，call：是否调用展开回调函数，unfold：是否强制指定节点的文件夹图标为展开
|nodeFold|node,call|nodeUnfold(getNodeById(0),false)|无|无|将指定节点打开|参数与nodeFold方法内参数一致
|hasChild|node|hasChild(getNodeById(0))|boolean|true|获取指定节点是否有子节点|略
|foldAll|node|foldAll(getNodeById(0))|无|无|展开指定节点下所有子节点|略
|addEventListener|elem,type,func|addEventListener(getNodeById(0).elem,'click',{回调函数})|无|无|绑定一个节点的指定事件|elem:Node对象的elem属性，type:事件名称,func:回调函数
|showHide|无|showHide()|无|无|将隐藏的节点显示出来|
|getChecked|无|getChecked()|Set|["1","2"]|返回所有选中的节点的id|
|checkClick|node|checkboxClick(getNodeById(0))|无|无|复选框点击事件
|reload|option|reload(option/不填写)|无|无|重新加载数据，插件的最外层div不会重新创建。建议只重新加载数据时调用此方法，否则可能存在丢失事件或其他情况，其他情况建议重新创建对象|参数与创建对象时一致，可为空，若为空则数据重新加载，其他不变
|clear|node|clear(node/不填写)|无|无|清空指定节点下所有子节点|node为空则移除所有
|remove|node|remove(node/不填写)|无|无|移除指定节点|node为空则移除所有

|属性名|类型|示例|描述|
|---|---|---|---|
|timeOut|int|100|设置单击按钮响应相隔时长（单双击区分时使用）
|format|json|format.id='cid'|格式化data的属性名，见参数的format参数

上述方法与属性均需要用实例化的对象去调用。其他方法尽量不要调用，其他方法不通用

### Node对象
|属性名/方法名|类型|返回类型|示例|描述|
|---|---|---|---|---|
|id|属性|int|1|该节点id
|isParent|属性|boolean|true|判断是否为父节点
|data|属性|json|{id:"1"...}|该节点的原始数据
|elem|属性|dom节点|<div ...>...</div\>|该节点的dom树元素，该div为显示字的div
|name|属性|string|节点1|该节点显示字符
|title|属性|string|节点1|该节点title属性字符
|checked|方法|boolean|true|获取该节点是否被选中
|childs|方法|数组|[Node...]|返回该节点所有子类，第一次调用会找一遍，第二次再调用该对象的childs()方法将直接返回
|parent|方法|Node对象|Node|返回该节点的父类，与childs方法一致
|next|方法|Node对象|Node|返回该节点下一个兄弟节点
|prev|方法|Node对象|Node|返回该节点上一个兄弟节点
|hasChild|方法|boolean|true|返回该节点是否含有子节点
|foldAll|方法|无|无|展开该节点下的所有节点，包括子节点（同tree的foldAll方法）
|addEventListener|方法|无|无|添加该节点的指定事件
|check|方法|无|无|模拟该节点的选中事件
|addNode|方法|无|无|在该节点下添加子节点（同tree的addNode方法）
|addNodeByInterface|方法|无|无|在该节点下通过接口添加子节点（同tree的addNodeByInterface方法）
|clear|方法|无|无|清空该节点下所有子节点
|remove|方法|无|无|移除该节点
|fold|方法|无|无|收缩该节点
|unFold|方法|无|无|展开该节点

以上说明内含有（同tree的XXX方法）的均为调用tree对应方法，详细用法请去tree的所有方法内查看

## 可扩展性
如需修改ui，请重新创建一个css样式，在引用tree.css后，引用重写的样式，调试完成之后可不引用tree.css

如需修改图片，则需先在img目录下新建一个文件夹，然后在这个文件夹内放入引用的图片，将图片改名为default目录相应的名字，

如不能改名，则需将tree.js文件，Icon属性下面url的文件名更改成相应的名字，最后在调用前，或直接修改tree.js文件下ui的值
为相应的文件夹名称

若图标显示不出来，更改tree.js文件下baseUrl的值即可。

## 兼容性
|浏览器 | 兼容性|备注|
|---|---| ---|
|IE| IE9 以上（包含）|工具栏在ie样式会变乱，不建议在IE打开工具栏选项
|谷歌|支持|
|火狐|支持|
|360|支持|
|Edge|支持|
|搜狗浏览器|支持|
|QQ浏览器|支持|


## 压缩说明
若后续有更新但压缩文件的未更新，请前往[https://tool.css-js.com/](https://tool.css-js.com/)，在该网页上压缩之后替换即可
本插件所有压缩均为该网站压缩
